# pera-trta-cli

CLI support for [pera-trta](https://www.npmjs.com/package/pera-trta)

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/pera-trta-cli.svg)](https://npmjs.org/package/pera-trta-cli)
[![Downloads/week](https://img.shields.io/npm/dw/pera-trta-cli.svg)](https://npmjs.org/package/pera-trta-cli)
[![License](https://img.shields.io/npm/l/pera-trta-cli.svg)](https://github.com/dragoljub-bogicevic-devtech/pera-trta-cli/blob/master/package.json)
