export enum FileSuffix {
	interface = '-interface',
	model = '-model',
	collection = '-collection'
}
