import { ITemplateReplaceData } from './templete-replace-data';

const fs = require('fs');

export function createTemplateFile(name: string, content: string): void {
	name = name.replace('.ts', '');
	fs.writeFile(`./${name}.ts`, content, (err: any) => {
		if (err) {
			return console.log(err);
		}
		console.log(`The file ${name}.ts was created!`);
	});
}

export function prepareTemplateContent(template: string, forReplace: ITemplateReplaceData[]): string {
	forReplace.forEach((r: ITemplateReplaceData) => {
		if (r) {
			template = template.replace('*', r.isFilePath ? r.content : prepareName(r.content));
		}
	});

	return template;
}

function prepareName(name: string) {
	name = name.replace('.ts', '');
	const splitParts = name.split('-');
	if (splitParts.length > 1) {
		return splitParts
			.map((part) => {
				return toUpperCaseFirstLetter(part.toLowerCase());
			})
			.join('');
	}

	return toUpperCaseFirstLetter(name.toLowerCase());
}

function toUpperCaseFirstLetter(data: string) {
	return data
		.split('')
		.map((el: string, i: number) => {
			if (i === 0) {
				el = el.toLocaleUpperCase();
			}
			return el;
		})
		.join('');
}
