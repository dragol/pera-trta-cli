import { Command, flags } from '@oclif/command';

import { createTemplateFile, prepareTemplateContent } from './helpers';
import { TemplateStrings } from './templates-strings';
import { FileSuffix } from './file-suffix';

class PeraTrtaCli extends Command {
	static description = 'Create interface, model and collection files.';

	static flags = {
		all: flags.string({ char: 'a', description: 'name for interface, model and collection' }),
		model: flags.string({ char: 'm', description: 'model name' }),
		interface: flags.string({ char: 'i', description: 'interface name' }),
		collection: flags.string({ char: 'c', description: 'collection name' })
	};

	async run() {
		const { flags } = this.parse(PeraTrtaCli);

		if (flags && flags.all) {
			const interfaceName = `${flags.all}${FileSuffix.interface}`;
			createTemplateFile(
				interfaceName,
				prepareTemplateContent(TemplateStrings.interface, [ { content: interfaceName, isFilePath: false } ])
			);

			const modelName = `${flags.all}${FileSuffix.model}`;
			createTemplateFile(
				modelName,
				prepareTemplateContent(TemplateStrings.model, [
					{ content: interfaceName, isFilePath: false },
					{ content: interfaceName, isFilePath: true },
					{ content: modelName, isFilePath: false },
					{ content: interfaceName, isFilePath: false }
				])
			);

			const collectionName = `${flags.all}${FileSuffix.collection}`;
			createTemplateFile(
				collectionName,
				prepareTemplateContent(TemplateStrings.collection, [
					{ content: modelName, isFilePath: false },
					{ content: modelName, isFilePath: true },
					{ content: interfaceName, isFilePath: false },
					{ content: interfaceName, isFilePath: true },
					{ content: collectionName, isFilePath: false },
					{ content: modelName, isFilePath: false },
					{ content: interfaceName, isFilePath: false }
				])
			);
		}

		if (flags && flags.model && flags.interface && flags.collection) {
			createTemplateFile(
				flags.interface,
				prepareTemplateContent(TemplateStrings.interface, [ { content: flags.interface, isFilePath: false } ])
			);

			createTemplateFile(
				flags.model,
				prepareTemplateContent(TemplateStrings.model, [
					{ content: flags.interface, isFilePath: false },
					{ content: flags.interface, isFilePath: true },
					{ content: flags.model, isFilePath: false },
					{ content: flags.interface, isFilePath: false }
				])
			);

			createTemplateFile(
				flags.collection,
				prepareTemplateContent(TemplateStrings.collection, [
					{ content: flags.model, isFilePath: false },
					{ content: flags.model, isFilePath: true },
					{ content: flags.interface, isFilePath: false },
					{ content: flags.interface, isFilePath: true },
					{ content: flags.collection, isFilePath: false },
					{ content: flags.model, isFilePath: false },
					{ content: flags.interface, isFilePath: false }
				])
			);
		}
	}
}

export = PeraTrtaCli;
