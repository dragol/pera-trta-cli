export const TemplateStrings = {
	model: `import { Model } from 'pera-trta';\n\nimport { * } from './*';\n\nexport class * extends Model<*> {}`,
	interface: `export interface * {}`,
	collection: `import { Collection } from 'pera-trta';\n\nimport { * } from './*';\nimport { * } from './*';\n\nexport class * extends Collection<*, *> {}`
};
