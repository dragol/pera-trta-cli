export interface ITemplateReplaceData {
	content: string;
	isFilePath: boolean;
}
